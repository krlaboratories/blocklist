### Facebook App

#### API?
b-api.facebook.com

#### MQTT Protocol 
edge-mqtt.facebook.com
mqtt-mini.facebook.com

#### Other
creative.ak.facebook.com
star-mini.c10r.facebook.com

### Hubspot & probably others

#### CTA Redirect

##### Information
Hubspot's CTAs are used to deliver personalized links matching a specific targeting audience and to track information about the success of this sort of marketing campeign (see [Hubspot knowledge base about their CTAs](https://knowledge.hubspot.com/cos-general/create-smart-calls-to-action-cta) and [Hubspot knowledge base about CTA tracking](https://knowledge.hubspot.com/cta/analyze-your-calls-to-action-performance)). Blocking this shouldn't break whole sites or services, however, it can prevent users from accessing certain links. If you're depended on services that use HubSpot's CTA links you can whitelist the appropriate domains to prevent them of being blocked.

##### Domains
cta-redirect.hubspot.com
